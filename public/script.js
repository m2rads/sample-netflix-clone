// put the check boxes here
const genresContaienr = document.getElementById("genres-container");
const searchForm = document.getElementById("search-form");

const resultsContainer = document.getElementById("results");

const populateGenres = () =>{
    axios.get("/api/genres")
    .then(result=> {
        result.data.forEach(genre => {
            genresContaienr.innerHTML += `
                <label>
                    ${genre}
                    <input type="checkbox" name="genres" value="${genre}" /> 
                </label>
            `;
        })
    })
    .catch(error=> {
        console.log(error);
    })
}

const handleSearchByGenre = (event)=> {
    event.preventDefault();

    // figure out which checkboxes are chosen
    let checkedBoxValues = [];

    // selects the elements on the documents and creates nodes on that
    // the # selects the cobntainer and the input element inside of it
    const checkboxNodes = document.querySelectorAll("#genres-container");

    for(let i = 0; i<checkboxNodes.length; i++) {
        if (checkboxNodes[i].checked == true) //checked property is fom checkboxes itself 
        {
            checkedBoxValues.push(checkboxNodes[i].value); 
        }
    }

    axios.get("api/movies", {params:{limit:30, genres:checkedBoxValues}})
    .then(result=> {
        // clearing the results table first and building the headings
        resultsContainer.innerHTML = `
            <tr>
                <th scope="col">
                    Title
                </th>
                <th scope="col>
                    year
                </th>
                <th scope="col">
                    Poster
                </th>
            </tr>
        `;
        result.data.forEach(movie=>{
            resultsContainer.innerHTML+= `
                <tr>
                    <td>${movie.title}</td>
                    <td>${movie.year}</td>
                    <td><img src="${movie.poster}" alt="poster" /></td>
                </tr>
            `;
        })
    })
    .catch(error=> {
        console.log(error);
    })
}

populateGenres();