const mongoose = require("mongoose");

let Schema = mongoose.Schema;

let MovieSchema = new Schema({
    title: {type: String, required: true, maxLength: 100},
    genres: [{type: String}],
    poster: String, //url for poster image
    year: {type: Number, min:0, max:9999}
});

exports.Movie = mongoose.model('Movie', MovieSchema, 'movies');

