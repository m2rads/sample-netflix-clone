const {Movie} = require('./models/Movie.js');

exports.getGenres = (req, res)=> {
    // send a list of genres after getting a list of distinct genres
    Movie.distinct("genres", (error, results)=>{
        //call back function
        if(error){
            res.status(500).send('error!');
        } else {
            res.send(results);
        }
    });
}

exports.getMovies = (req, res)=> {
    // send a list of movies based on some paramters
    // in particular, genres, limit, skip 
    // /api/movie?limit=40&skip=200&genres[]=Action&genres[]=comedy
    
    // paramGenres is an arrya of genrees that the user is searhcing for

    let limit = parseInt(req.query.limit || 10);
    let skip = parseInt(req.query.skip || 0);

    let paramGenres = req.query.genres;
    let dbQuery = {}; 

    if (paramGenres != undefined && paramGenres.length > 0) {
        dbQuery = {genres:{$in:paramGenres}};
    } 

    Movie.find(dbQuery, "title year poster")
    .skip(skip)
    .limit(limit)
    .exec((error, results)=> {
        if(error){
            res.status(500).send('error in fetching movie!');
        } else {
            res.send(results);
        }
    });
}