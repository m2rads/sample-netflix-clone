const express = require("express");
const app = express();

const {db} = require('./connection.js');
const {getGenres, getMovies} = require('./routeHandlers.js');

// database connection test
db.once("open", ()=> {
    console.log("connected to database");
    const server = app.listen(process.env.PORT || 8080, ()=> 
        console.log("listening on 8080"));
});

// adding middleware to serve static files
// this middleware automatically makes an end point
//to access all the static files inside the public folder

app.use(express.static("public"));

// API routes
app.get('/api/genres', getGenres);
app.get('/api/movies', getMovies);






